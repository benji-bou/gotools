package printutil

import (
	"encoding/json"

	"github.com/labstack/gommon/color"
)

func Printf(format string, v ...interface{}) {
	prettyV := make([]interface{}, len(v))
	for i, rawV := range v {
		json, err := json.MarshalIndent(rawV, "", "   ")
		if err == nil {
			prettyV[i] = string(json)
		}
	}

	color.Printf(format, prettyV...)
}
