package timeutil

import (
	// "encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	// "log"
	// "log"
	"strconv"
	"time"
)

type UnixTime time.Time

func (t UnixTime) MarshalJSON() ([]byte, error) {
	// b := make([]byte, 8)
	realTime := time.Time(t)
	return json.Marshal(realTime)
	// binary.PutVarint(b, realTime.Unix())
	// log.Println("MarshalJSON---------->", b)
	// return b, nil
}

func (t *UnixTime) UnmarshalJSON(b []byte) error {
	// you can now parse b as thoroughly as you want
	var dateUnix int64
	errUnix := json.Unmarshal(b, &dateUnix)

	if errUnix == nil {
		*t = UnixTime(time.Unix(dateUnix, 0))
		return nil
	}
	tmpTime := &time.Time{}
	err := json.Unmarshal(b, tmpTime)
	if err == nil {
		*t = UnixTime(*tmpTime)
		return nil
	}
	return errors.New(fmt.Sprint(err, errUnix))
}

func (t *UnixTime) UnmarshalParam(param string) error {
	tmpTime, err := time.Parse(time.RFC3339, param)
	if err == nil {
		*t = UnixTime(tmpTime)
		return nil
	}
	baseInt, errInt := strconv.ParseInt(param, 10, 64)
	if errInt == nil {
		*t = UnixTime(time.Unix(baseInt, 0))
		return nil
	}
	return errors.New(fmt.Sprint(err, " --> ", errInt))
}

func Now() UnixTime {
	return UnixTime(time.Now())
}
